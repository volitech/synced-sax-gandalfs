const video             = document.getElementsByTagName("video")[0];
const CYCLE_DURATION    = 7343;
const SLOW_RATE         = .9;
const SPEED_UP_RATE     = 1.1;
const LOOP_INTERVAL     = 227030;
const DRIFT_TIMEOUT     = 100;

video.addEventListener("click", init);

document.getElementById("fullscreen-button").addEventListener("click", () => document.body.requestFullscreen());
document.getElementById("help-button").addEventListener("click", () => togglePopup(true));
document.getElementById("close-button").addEventListener("click", () => togglePopup(false));
document.getElementById("overlay").addEventListener("click", () => togglePopup(false));

document.addEventListener("fullscreenchange", () => {
    document.getElementById("navbar").style.visibility =
        document.fullscreenElement ? "collapse" : "visible";
});

function init() {
    const button = document.getElementById("play-button");

    button.classList.add("active");

    video.removeEventListener("click", init);
    video.addEventListener("timeupdate", synchronizeVideo);

    video.play();
    video.speed = 2;

    setInterval(() => {
        video.currentTime = 0;
        setTimeout(() => {
            video.addEventListener("timeupdate", synchronizeVideo);
        }, DRIFT_TIMEOUT);
    }, LOOP_INTERVAL);
}

function synchronizeVideo() {
    const current_time  = Math.round(video.currentTime * 1e3) % CYCLE_DURATION;
    const target_time   = Date.now() % CYCLE_DURATION;
    const time_diff     = (CYCLE_DURATION + target_time - current_time) % CYCLE_DURATION;
    const slow_timeout  = Math.abs((CYCLE_DURATION - time_diff) / (1 - SLOW_RATE));
    const fast_timeout  = time_diff / (SPEED_UP_RATE - 1);
    const timeout       = Math.min(slow_timeout, fast_timeout);
    const rate          = [SLOW_RATE, SPEED_UP_RATE][+(timeout == fast_timeout)];

    video.removeEventListener("timeupdate", synchronizeVideo);

    console.log(
        `I am behind by ${time_diff / 1e3}s;`,
        `Speeding ${["down", "up"][+(timeout == fast_timeout)]}`,
        `for ${Math.trunc(timeout) / 1e3}s`
    );

    video.playbackRate = rate;

    setTimeout(() => {
        console.log("SYNCED!");
        video.playbackRate  = 1;
    }, timeout);
}

function togglePopup(is_visible) {
    const class_list = document.getElementById("help-popup").classList

    if (is_visible)
        return class_list.add("visible");
    class_list.remove("visible");
}
